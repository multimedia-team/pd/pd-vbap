pd-vbap (1.2.1-2) unstable; urgency=medium

  * Re-build for both single and double-precision Pd

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 05 Jul 2023 15:41:45 +0200

pd-vbap (1.2.1-1) unstable; urgency=medium

  * New upstream version 1.2.1
  * Bump standards version to 4.6.2

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 13 Jun 2023 18:12:14 +0200

pd-vbap (1.2.0-3) unstable; urgency=medium

  * Remove superfluous deletion of LICENSE.txt prior to symlinking it.
  * Modernize 'licensecheck' target
    + Ensure that 'licensecheck' is run with the C.UTF-8 locale
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 07 Dec 2022 10:53:29 +0100

pd-vbap (1.2.0-2) unstable; urgency=medium

  * Switch to dh-sequence-pd-lib-builder
  * Regenerate d/copyright_hints
    + Exclude debian/ from licensecheck
    + Regenerate d/copyright_hints
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 06 Dec 2022 15:19:02 +0100

pd-vbap (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Update to the new upstream build-system
  * Drop Recommendation of pd-pddp
  * Fix path to upstream repository
  * Remove obsolete file d/source/local-options
  * Declare that building this package does not require 'root' powers.
  * Add salsa-ci configuration
  * d/watch: bump to version 4 and use @DEB_EXT@
  * wrap-and-sort -ast
  * Refresh d/copyright
    * Regenerate d/copyright_hints
  * Removed Hans-Christoph Steiner from Uploaders as he requested
  * Bump standards version to 4.6.0

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 13.
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.


 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 27 Jan 2022 12:28:13 +0100

pd-vbap (1.1-2) unstable; urgency=medium

  * Simplified & unified d/rules
    * Enabled hardening
    * Bumped dh compat to 11
  * Switched URLs to https://
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
  * Updated d/copyright(_hints)
  * Removed trailing whitespace in debian/*
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:42:42 +0100

pd-vbap (1.1-1) unstable; urgency=medium

  * New upstream version 1.1

  [ Hans-Christoph Steiner ]
  * Updated to copyright-format/1.0
  * Removed 'DM-Upload-Allowed: yes', its deprecated

  [ IOhannes m zmölnig ]
  * Dropped internal headers, as they conflict with the real ones
    (Closes: #872682)
  * Added myself to uploaders
  * Modernized Vcs-* stanzas
  * Recommend pd-pddp
  * Fixed license name and dropped empty upstream-contact field
  * Updated upstream source to github
  * Updated d/watch to github
  * Bumped dh compat to 10
  * Bumped standards version to 4.1.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 22 Aug 2017 18:57:31 +0200

pd-vbap (1.0.3.2-1) unstable; urgency=low

  * added Depends to real 'puredata-core' package in addition the virtual 'pd'
  * bumped standards version to 3.9.3
  * updated to latest upstream release

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 25 Jun 2012 17:12:59 -0400

pd-vbap (1.0.3.1-2) unstable; urgency=low

  [ Alexandre Quessy ]
  * Added Vcs-* stanzas.

  [ IOhannes m zmölnig ]
  * Fixed debian/watch

  [ Hans-Christoph Steiner ]
  * updated Build-Depends to use puredata-dev (Closes: #629802)
  * bumped standards version to 3.9.2

 -- Hans-Christoph Steiner <hans@eds.org>  Fri, 10 Jun 2011 14:46:55 -0400

pd-vbap (1.0.3.1-1) unstable; urgency=low

  * Initial release (Closes: #603198)

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 23 Nov 2010 17:10:29 -0500
